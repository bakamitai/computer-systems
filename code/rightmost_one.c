#include <stdio.h>

/*
 * Generate mask indicating righmost 1 in x.
 * For example 0xFF00 -> 0x0100 and 0x6600 --> 0x0200.
 * If x == 0, then return 0.
 */
/* int rightmost_one(unsigned x) */
/* { */
/*     int i; */
/*     int num_bits = 8 * sizeof(unsigned); */
/*     for(i = 0; i < num_bits; ++i) */
/*     { */
/*         if(x >> i & 1) break; */
/*     } */
/*     return 1 << i; */
/* } */

int rightmost_one(unsigned x)
{
    return -x & x;
}

int main()
{
    printf("%x\n", rightmost_one(0xFF00));
    printf("%x\n", rightmost_one(0x6600));
    return 0;
}
