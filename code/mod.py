# Calculate a mod b for all values of a and b
def mod(a, b):
    if a > 0 and b > 0:
        while a >= b:
            a -= b
        return a

    if a < 0 and b > 0:
        while a < 0:
            a += b
        return a

    if a > 0 and b < 0:
        while a > 0:
            a += b
        return a

    if a < 0 and b < 0:
        while a <= b:
            a -= b
        return a

    return 0


for i in range(-100, 100):
    for j in range(-100, 100):
        if j == 0:
            continue
        if mod(i, j) != i % j:
            print(f"{i} mod {j} is { i % j} not {mod(i, j)}")
