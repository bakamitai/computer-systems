#include <stdio.h>

int main()
{
    int not_exact = (1 << 24) + 1;
    float silly = (float) not_exact;
    printf("%d = %f???\n", not_exact, silly);
    return 0;
}
