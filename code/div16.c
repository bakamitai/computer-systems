#include <stdio.h>

int div16(int x)
{
    int negative = x >> 31; // All zeros if x >= 0, all ones if x < 0
    return (x + (negative & ((1 << 4) - 1))) >> 4;
}

int main()
{
    printf("%d / 16 = %d\n%d / 16 = %d\n%d / 16 = %d\n%d / 16 = %d\n", 32, div16(32), 33, div16(33), -32, div16(-32), -33, div16(-33));
    return 0;
}
