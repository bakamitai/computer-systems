#include <stdio.h>

int main()
{
    unsigned int x = 0x87654321;
    printf("%.8x\n%.8x\n%.8x\n", x & 0xFF, x ^ ~0xFF, x | 0xFF);
    return 0;
}
