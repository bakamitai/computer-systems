#include <stdio.h>

int negate(int x)
{
    int result = 0;
    for(int i = 0; i < 32; ++i)
    {
        result += (x << i);
    }
    return result;
}

int main()
{
    int x = 34;
    printf("negative %d = %d\n", x, negate(x));
    return 0;
}
