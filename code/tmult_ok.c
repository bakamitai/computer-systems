#include <stdio.h>
#include <stdint.h>

int tmult_ok(int x, int y)
{
    int64_t result;
    result = (int64_t) x * (int64_t) y;
    int high_order_bytes = result >> 32;
    int low_order_bytes = result;
    return high_order_bytes == 0 || high_order_bytes == -1 && low_order_bytes < 0;
}

int main()
{
    int x = 2;
    int y = 0x80000000;
    if(tmult_ok(x, y)) printf("It doesn't work\n");
    x = -5;
    y = 2054;
    if(tmult_ok(x, y)) printf("%d * %d = %d\n", x, y, x * y);
    return 0;
}
