#include <stdio.h>

int mod(int a, int b)
{
    while(a < 0) a += b;
    while(a >= b) a -= b;
    return a;
}

int main()
{
    for(int i = -16; i < 17; i++)
        printf("+----");
    putchar('+');
    putchar('\n');
    for(int i = -16; i < 17; i++)
        printf("|%4d", i);
    printf("| number line [-16, 16]\n");
    for(int i = -16; i < 17; i++)
        printf("+----");
    putchar('+');
    putchar('\n');
    for(int i = -16; i < 17; i++)
        printf("|%4d", mod(i, 3));
    printf("| number line [-16, 16] mod 3\n");
    for(int i = -16; i < 17; i++)
        printf("+----");
    putchar('+');
    putchar('\n');
    return 0;
}
