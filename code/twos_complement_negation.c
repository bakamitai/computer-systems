#include<stdio.h>

int tadd_ok(int x, int y)
{
    if(x >= 0 && y <= 0 || x <= 0 && y >= 0) return 1;
    else if(x < 0 && y < 0) return (x + y) < 0;
    else return (x + y) > 0;
}

int tsub_ok(int x, int y)
{
    return tadd_ok(x, -y);
}

int neg(int x)
{
    return (~x) + 1;
}

int incr(int x)
{
    unsigned ones = ~0;
    for(int i = 0; i < 32; i++)
    {
        if(!((x >> i) & 1)) return x ^ (ones >> 31 - i);
    }
    return 0;
}

int main()
{
    unsigned ux = ~0;
    ux = ~(ux >> 1);
    int y = ux;
    int x = 5;
    if(tsub_ok(x, y)) printf("x = %d, y = %d, x - y = %d\n", x, y, x - y);
    putchar('\n');
    putchar('\n');
    putchar('\n');

    int a = 15;
    printf("a = %d, BUT -a = %d, BUT -(-a) = %d\n", a, neg(a), neg(neg(a)));
    putchar('\n');
    putchar('\n');
    putchar('\n');

    int b = -2;
    printf("b = %d, BUT b + 1 = %d, BUT b + 1 + 1 = %d\n", b, incr(b), incr(incr(b)));
    return 0;
}
