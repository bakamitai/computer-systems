#include <stdio.h>
#include <stdint.h>

void show_bytes(unsigned char *byte_pointer, size_t size)
{
    while(size--) printf("%.2x ", *byte_pointer++);
    putchar('\n');
}

int is_little_endian()
{
    uint16_t test = 0;
    uint8_t *byte = (uint8_t *) &test;
    *byte = 1;
    return test == 1;
}

unsigned replace_byte(unsigned x, int i, unsigned char b)
{
    unsigned zero_byte = ~(0xFF << i * 8);
    x &= zero_byte;
    x |= b << i * 8;
    return x;
}

int main()
{
    /* Epic zero/sign extension */
    short sx = -12345;
    unsigned short usx = sx;
    int x = sx;
    unsigned int ux = usx;

    printf("sx  = %d:\t", sx);
    show_bytes((unsigned char *) &sx, sizeof(short));
    printf("usx = %u:\t", usx);
    show_bytes((unsigned char *) &usx, sizeof(unsigned short));
    printf("x   = %d:\t", x);
    show_bytes((unsigned char *) &x, sizeof(int));
    printf("ux  = %u:\t", ux);
    show_bytes((unsigned char *) &ux, sizeof(unsigned int));

    putchar('\n');
    /* Epic truncation */
    x = 53191;
    sx = x;
    printf("x  = %d\t", x);
    show_bytes((unsigned char *) &x, sizeof(int));
    printf("sx = %d\t", sx);
    show_bytes((unsigned char *) &sx, sizeof(short));

    putchar('\n');
    /* Epic fixed size modular arithmetic */
    unsigned char a = 1 << 7;
    unsigned char b = 1 << 7;
    unsigned char c = a + b;
    printf("a     = %u:\t", a);
    show_bytes(&a, 1);
    printf("b     = %u:\t", b);
    show_bytes(&b, 1);
    printf("a + b = %u:\t", c);
    show_bytes(&c, 1);

    if(is_little_endian()) printf("\n\nYES! FUCK BABY! Little Endian BABY FUCK!\n");
    unsigned xx = 0x89ABCDEF;
    unsigned yy = 0x76543210;

    unsigned result = (xx & 0xFF) | (yy & ~0xFF);
    printf("%x\n", result);

    printf("Question 2.60\n\n");
    printf("Expecting: 0x12ab5678\nGot:       0x%x\n", replace_byte(0x12345678, 2, 0xAB));
    printf("\nExpecting: 0x123456ab\nGot:       0x%x\n", replace_byte(0x12345678, 0, 0xAB));

    int kek = -8;
    printf("\n\n%d\n", x && 1);
    return 0;
}
