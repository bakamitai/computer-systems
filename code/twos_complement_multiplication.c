#include <stdio.h>

int main()
{
    int x = 0x80000000;
    int y = 0x7FFFFFFF;
    int p = x * y;
    if(p == x) printf("You are right :)\n");
    else printf("You are wrong >:(\n");

    x = 0x7FFFFFFF;
    y = 0x7FFFFFFF;
    p = x * y;
    if(p == 1) printf("You are right :3\n");
    else printf("You are wrong >:[\n");
    return 0;
}
