#include <stdio.h>

int int_shifts_are_arithmetic()
{
    /* Get number of bits for data type int */
    int word_bits = sizeof(int) << 3;

    /* Generate w bit int with 1 as it's most significant bit and all other bits 0 */
    int x = 1 << (word_bits - 1);

    /* Generate w bit int that is the result of shifting x right by one */
    int x_shifted = x >> 1;

    /* If ints are shifted arithmetically x_shifted will have been sign extended and have the 
     * it's two most significant bits equal to 1 and the rest 0. If ints are shifted logically
     * x_shifted will have it's most significant bit equal to 0 the second most significant bit
     * equal 1 and the rest equal 0. The value of x_shifted if arithmetic shifting is used will
     * be -2**(w-1) + 2**(w-2).
     */
    return x_shifted == x + (1 << (word_bits - 2));
}

int main()
{
    #if 0
    int x;
    long i;
    for(i = 0, x = 1; x && 1; x++, i++);
    printf("Should be 4294967295. %ld\n", i);

    for(i = 0, x = 0; (x ^ -1) && 1; x++, i++);
    printf("Should be 4294967295. %ld\n", i);

    x = 0;
    i = 0;
    for(int j = 0; j < (1 << 24); j++)
    {
        for(int k = 0; k < 256; k++)
        {
            x = (j << 8) | k;
            i += (x & 0xFF) && 1;
        }
    }
    printf("Should be 4278190080. %ld\n", i);

    x = 0;
    i = 0;
    for(int j = 0; j < (1 << 24); j++)
    {
        for(int msb = 0; msb < 256; msb++)
        {
            x = msb << 24 | j;
            i += ((x | ~(0xFF << ((sizeof(int) - 1) << 3))) ^ -1) && 1;
        }
    }
    printf("Should be 4278190080. %ld\n", i);
    #endif
    if(int_shifts_are_arithmetic()) printf("Yes they are! B^)\n");
    else printf("No they AREN'T!!! >:[\n");
}

/*
2**24 * (2**8 - 1) = 2**(24 + 8) - 2**24
2**32 - 2**24
 */
