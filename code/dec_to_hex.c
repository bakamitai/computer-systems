#include <stdio.h>
#include <stdlib.h>

char *parse_int_to_hex(unsigned long n, char *buffer)
{
    while(n > 0)
    {
        unsigned char r = n % 16;
        if(r < 10) *buffer-- = '0' + r;
        else *buffer-- = 'A' + (r - 10);
        n /= 16;
    }
    return buffer + 1;
}

int main(int argc, char **argv)
{
    char _buffer[64];
    _buffer[63] = '\0';
    char *buffer = _buffer;
    buffer += 62;
    long n = atol(argv[1]);
    puts(parse_int_to_hex(n, buffer));
    return 0;
}
