#include <stdio.h>
#include <inttypes.h>

typedef unsigned char * BytePointer;

void show_bytes(BytePointer b, size_t len)
{
    while(len--)
    {
        printf("%.2x ", *b++);
    }
    putchar('\n');
}

uint16_t binary_to_unsigned16(char *vector)
{
    uint16_t result = 0;
    uint16_t one = 1;
    for(int i = 0; i < 16; ++i)
    {
        result += vector[15 - i] * (one << i);
    }
    return result;
}

int16_t binary_to_twos_complement16(char *vector)
{
    int16_t result = 0;
    uint16_t one = 1;
    for(int i = 0; i < 15; ++i)
    {
        result += vector[15 - i] * (one << i);
    }
    return result - vector[0] * (one << 15);
}

int main()
{
    short x = 12345;
    short mx = -x;
    show_bytes((BytePointer) &x, sizeof(short));
    show_bytes((BytePointer) &mx, sizeof(short));
    char vector[16] = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1};
    printf("Binary to two's complement of [0011 0000 0011 1001] = %d\n", binary_to_twos_complement16(vector));
    char another_vector[16] = {1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1};
    printf("Binary to two's complement of [1100 1111 1100 0111] = %d\n", binary_to_twos_complement16(another_vector));
    return 0;
}
