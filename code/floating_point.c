#include <stdio.h>
#include <stdlib.h>

float floating_point_science_lab_make_me_one()
{
    unsigned subject = 126;       // Exponent must be -1. Bias is 127, 126 - 127 = -1.
    subject <<= 23;               // Put bits where they belong
    subject |= (1 << 23) - 1;     // Fractional part must be 2. 2*2^(-1) = 2/2 = 1. Closest to 2 is all 1's
    return *((float *) &subject);
}

float floating_point_science_lab_largest_normalised_value()
{
    unsigned subject = 0xFE << 23;
    subject |= (1 << 23) - 1;
    return *((float *) &subject);
}

void bubble_sort(int *array, int length)
{
    for(int i = 0; i < length - 1; ++i)
    {
        for(int j = 0; j < length - 1 - i; ++j)
        {
            if(array[j] > array[j + 1])
            {
                // Funny little xor nonsense. array[j] and array[j + 1] will never have the
                // same memory address so this should always work.
                array[j]     = array[j] ^ array[j + 1];
                array[j + 1] = array[j] ^ array[j + 1];
                array[j]     = array[j] ^ array[j + 1];
            }
        }
    }
}

int main(int argc, char **argv)
{
    if(argc == 2)
    {
        float x = atof(argv[1]);
        unsigned x_bits = *((unsigned *) &x);
        int s = x_bits >> 31;
        int exp = (x_bits >> 23) & 0xFF;
        int frac = x_bits & ((1 << 23) - 1);
        float frac_value = 1.0 + (float) frac / (float) (1 << 23);
        int bias = 127;

        printf("floating point number = %f\ns = %d\nexp = %d\nfrac = %f\n", x, s, exp - bias, frac_value);
        printf("\nIs the science correct? This should be 1.0, %f\n", floating_point_science_lab_make_me_one());
        printf("\nIs the science correct? This should be the largest normalised value, %f\n", floating_point_science_lab_largest_normalised_value());

        float numbers[10] = {9.0, 4.0, 1.0, 3.0, 7.0, 2.0, 8.0, 5.0, 6.0, 10.0};
        bubble_sort((int *) numbers, 10);
        for(int i = 0; i < 10; i++)
        {
            printf("%f\n", numbers[i]);
        }
    }
    double d = 43.0;
    float f = 1.0/0.0;
    if((f + d) - f == d) printf("Gabagool!\n");
    else printf("Wababababadeeee!\n");

    float positive_infinity = 1.0 / 0.0;
    float negative_infinity = -positive_infinity;
    float not_a_number = positive_infinity + negative_infinity;
    if(not_a_number * not_a_number >= 0) printf("They were right :'(\n%f * %f is greater than or equal to 0\n", not_a_number, not_a_number);
    else printf("I was right baby!!! Woo!! I was fucking right baby fuck! :D\n");
    return 0;
}
